import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df2 = df.set_index('Name')

    df3 = df2.filter(like = 'Mr.', axis = 0)
    mr = df3['Age'].median()
    mr_n = df3['Age'].isna().sum()

    df4 = df2.filter(like = 'Mrs.', axis = 0)
    mrs = df4['Age'].median()
    mrs_n = df4['Age'].isna().sum()

    df5 = df2.filter(like = 'Miss.', axis = 0)
    miss = df5['Age'].median()
    miss_n = df5['Age'].isna().sum()

    return [('Mr.', mr_n, mr), ('Mrs.', mrs_n, mrs), ('Miss.', miss_n, miss)]
